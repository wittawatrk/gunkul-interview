/* eslint-disable no-console */

export default function (ctx) {
  console.log()
  ctx.$axios.setBaseURL(encodeURI(`http://localhost:3000/`))
  ctx.$axios.onRequest((config) => {
    // check if the user is authenticated
    console.log(ctx.store.state.auth.access_token)
    if (ctx.store.state.auth.access_token) {
      // set the Authorization header using the access token
      config.headers.Authorization =
        'Bearer ' + ctx.store.state.auth.access_token
    }
    console.log(config.headers.Authorization)
    return config
  })

  ctx.$axios.onError((error) => {
    return new Promise(function (resolve, reject) {
      console.log(error)
      // ignore certain paths (i.e. paths relating to authentication)

      const statusCode = error.response ? error.response.status : -1
      console.log(statusCode)
      // only handle authentication errors or errors involving the validity of the token
      if (statusCode === 401 || statusCode === 422) {
        // API should return a reason for the error, represented here by the message property

        // Example API response:
        // {
        //   status: 'failed',
        //   message: 'TOKEN_EXPIRED',
        //   message: 'The JWT token is expired',
        //   status_code: 401
        // }

        // retrieve the message property from the response, or default to null
        const { data: { message } = { message: null } } = error.response || {}
        console.log(message)
        // get the refresh token from the state if it exists
        const refreshToken = ctx.store.state.auth.refresh_token

        // determine if the error is a result of an expired access token
        // also ensure that the refresh token is present
        if (message === 'TOKEN_EXPIRED' && refreshToken) {
          // see below - consider the refresh process failed if this is a 2nd attempt at the request
          // eslint-disable-next-line no-prototype-builtins
          if (error.config.hasOwnProperty('retryAttempts')) {
            // immediately logout if already attempted refresh
            ctx.store.dispatch('auth/logout')

            // redirect the user home
            return ctx.redirect('/')
          } else {
            // merge a new retryAttempts property into the original request config to prevent infinite-loop if refresh fails
            const config = { retryAttempts: 1, ...error.config }

            try {
              // attempt to refresh access token using refresh token
              ctx.store.dispatch('auth/refresh')

              // re-run the initial request using the new request config after a successful refresh
              // this response will be returned to the initial calling method
              return resolve(ctx.$axios(config))
            } catch (e) {
              // catch any error while refreshing the token
              ctx.store.dispatch('auth/logout')

              // redirect the user home
              return ctx.redirect('/')
            }
          }
        } else if (message === 'Unauthorized') {
          // catch any other JWT-related error (i.e. malformed token) and logout the user
          ctx.store.dispatch('auth/logout')

          // redirect the user home
          return ctx.redirect('/')
        }
      }

      // ignore all other errors, let component or other error handlers handle them
      return reject(error)
    })
  })
}
