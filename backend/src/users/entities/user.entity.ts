import {Entity, Column, PrimaryGeneratedColumn, BeforeInsert, Unique} from 'typeorm';
import * as crypto from 'crypto'
@Entity()
export class User {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    firstName: string;

    @Column()
    lastName: string;

    @Column({unique:true})
    email: string;
    @Column()
    password:string;
    @BeforeInsert()
    async hashPassword() {
        this.password = crypto.createHmac('sha256', this.password).digest('hex');

        console.log(this.password)
    }
}
