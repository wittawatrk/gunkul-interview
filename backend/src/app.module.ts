import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UsersModule } from './users/users.module';
import {TypeOrmModule} from  '@nestjs/typeorm'
import {User} from "./users/entities/user.entity";
import { ProductsModule } from './products/products.module';
import {Product} from "./products/entities/product.entity";
import {AuthModule} from "./auth/auth.module";
import {ServeStaticModule} from "@nestjs/serve-static";
import { join } from 'path'
@Module({
  imports: [ TypeOrmModule.forRoot({
    type: 'mariadb',
    host: 'mysql',
    port: 3306,
    username: 'admin',
    password: 'admin',
    database: 'gunkul',
    entities: [User,Product],
    synchronize: true,
  }), UsersModule, ProductsModule,AuthModule,
  ServeStaticModule.forRoot({
    rootPath: join(__dirname, '..', 'image'),
  }),
],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
