import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import {User} from "../users/entities/user.entity";
import { UsersService } from '../users/users.service';
import { JwtModule } from '@nestjs/jwt';
import { AuthService } from './auth.service';
import { AuthController } from './auth/auth.controller';
import { JwtStrategy } from './passport/jwt.strategy';
import { PassportModule } from '@nestjs/passport';

@Module({
    imports: [TypeOrmModule.forFeature([User]),
    JwtModule.register({
        secret: 'secret12356789'
    }),PassportModule
    ],
    providers: [UsersService, AuthService,JwtStrategy],
    exports:[UsersService, AuthService,JwtStrategy],
    controllers: [AuthController]
})
export class AuthModule { }
