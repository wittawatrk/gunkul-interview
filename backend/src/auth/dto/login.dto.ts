import {Column, Double, PrimaryGeneratedColumn} from "typeorm";
import {ApiProperty} from "@nestjs/swagger";

export class LoginDto {

    @ApiProperty()
    email: string;
    @ApiProperty()
    password: string;
}
