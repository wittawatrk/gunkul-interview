import {HttpException, HttpStatus, Injectable} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import {UsersService} from "../users/users.service";
import {User} from "../users/entities/user.entity";
import { CreateUserDto } from '../users/dto/create-user.dto';
import {LoginDto} from "./dto/login.dto";
import * as crypto from "crypto";

@Injectable()
export class AuthService {
    constructor(
        private readonly userService: UsersService,
        private readonly jwtService: JwtService
    ) { }

    async validate(userData: LoginDto): Promise<User> {
        // compare passwords
      const user =  await this.userService.findByEmail(userData.email);
      if(!user){
          throw new HttpException('User with this email does not exist', HttpStatus.NOT_FOUND);

      }
        const areEqual = crypto.createHmac('sha256', userData.password).digest('hex') === user.password;
        if (!areEqual) {
            throw new HttpException('Invalid credentials', HttpStatus.NOT_FOUND);
        }
        return user
    }

    public async login(user: LoginDto): Promise< any | { status: number }>{
        console.log(user)
        return this.validate(user).then((userData)=>{
          if(!userData){
            return { status: 404 };
          }
          delete userData.password
          const payload = JSON.stringify(userData);
          console.log(userData)
          const accessToken = this.jwtService.sign(payload);

          return {
             expires_in: 3600,
             access_token: accessToken,
             user: payload,
             status: 200
          };

        });
    }

    public async register(user: CreateUserDto): Promise<any>{
        return this.userService.create(user)
    }
    async validateUser(payload: any): Promise<any> {
        return await this.userService.findOne(payload.id)
    }

}
