import { ExtractJwt,Strategy } from 'passport-jwt';
import { AuthService } from '../auth.service';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable, UnauthorizedException } from '@nestjs/common';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
    constructor(private readonly authService: AuthService) {
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            secretOrKey: 'secret12356789',
        });
    }
    // tslint:disable-next-line:ban-types
    async validate(payload: string) {
        const user = await this.authService.validateUser(payload);
      // tslint:disable-next-line:no-console
        if (!user) {
            throw  new UnauthorizedException();
        }
        return user;
    }
}
