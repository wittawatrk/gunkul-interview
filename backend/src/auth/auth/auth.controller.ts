import { Controller, Post, Body } from '@nestjs/common';
import { AuthService } from '../auth.service';
import {User} from "../../users/entities/user.entity";
import {UsersService} from "../../users/users.service";
import {ApiResponse, ApiTags} from "@nestjs/swagger";
import {LoginDto} from "../dto/login.dto";
@ApiTags('Authentication')
@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService, private userService: UsersService) {
  }
  @ApiResponse({status:200,description:'return Detail and token'})
  @Post('login')
  async login(@Body() user : LoginDto): Promise<any> {
    return this.authService.login(user);
  }
}
