import { PartialType } from '@nestjs/mapped-types';
import { CreateProductDto } from './create-product.dto';
import {ApiProperty} from "@nestjs/swagger";

export class UpdateProductDto extends PartialType(CreateProductDto) {
    @ApiProperty()
    name: string;

    @ApiProperty()
    price: number;

    @ApiProperty()
    description: string;

    @ApiProperty(
        {
            description: 'Attachments',
            type:'file',
            items:{
                type:'string',
                format:'binary'
            }
        }
    )
    image:any;
}
