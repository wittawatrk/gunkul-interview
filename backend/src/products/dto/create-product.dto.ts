import {Column, Double, PrimaryGeneratedColumn} from "typeorm";
import {ApiProperty} from "@nestjs/swagger";

export class CreateProductDto {
    id: number;
    @ApiProperty()
    name: string;

    @ApiProperty()
    price: number;

    @ApiProperty()
    description: string;

    @ApiProperty(
        {
            description: 'Attachments',
            type:'file',
            items:{
                type:'string',
                format:'binary'
            }
        }
    )
    image:any;
}
