import { Injectable } from '@nestjs/common';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import {InjectRepository} from "@nestjs/typeorm";
import {Repository} from "typeorm";
import {Product} from "./entities/product.entity";

@Injectable()
export class ProductsService {
  constructor(
      @InjectRepository(Product)
      private productsRepository: Repository<Product>,
  ) {}
 async create(createProductDto: CreateProductDto,file) : Promise<Product>{
     createProductDto.image = file.path
    const product = await this.productsRepository.create(createProductDto)
     await this.productsRepository.save(createProductDto)
    return product;
  }

  findAll():Promise<Product[]> {
    return this.productsRepository.find();
  }

  findOne(id: number) {
      return this.productsRepository.findOne(id);
  }

 async update(id: number, updateProductDto: UpdateProductDto,image:any) {
      const product = await this.productsRepository.findOne(id)
     if(image.path) product.image = image.path
     if(updateProductDto.description) product.description = updateProductDto.description
     if(updateProductDto.name) product.name = updateProductDto.name
     if(updateProductDto.price) product.price = updateProductDto.price
     return await this.productsRepository.save(product);
  }

  remove(id: number) {
    return this.productsRepository.delete(id);
  }
}
