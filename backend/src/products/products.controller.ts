import {
  Controller,
  Get,
  Post,
  Body,
  Put,
  Param,
  Delete,
  UseInterceptors,
  UploadedFile,
  UseGuards, Res
} from '@nestjs/common';
import { ProductsService } from './products.service';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import {ApiBearerAuth, ApiConsumes, ApiTags} from "@nestjs/swagger";
import {FileInterceptor} from "@nestjs/platform-express";
import {diskStorage} from 'multer'
import {AuthGuard} from "@nestjs/passport";

@ApiTags('Product')
@Controller('products')
export class ProductsController {
  constructor(private readonly productsService: ProductsService) {}
  @Post()
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiConsumes('multipart/form-data')
  @UseInterceptors(FileInterceptor('image' ,{
    storage: diskStorage({
      destination: './image',
      filename:(req,file,cb)=>{
        return cb(null,`${new Date().getTime()}${file.originalname}`)
      }
      })
  }))
  create(@Body() createProductDto: CreateProductDto,@UploadedFile() image,)  {
    return this.productsService.create(createProductDto,image);
  }

  @Get()
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  findAll() {
    return this.productsService.findAll();
  }

  @Get(':id')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  findOne(@Param('id') id: string) {
    return this.productsService.findOne(+id);
  }

  @Put(':id')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiConsumes('multipart/form-data')
  @UseInterceptors(FileInterceptor('image' ,{
    storage: diskStorage({
      destination: './image',
      filename:(req,file,cb)=>{
        return cb(null,`${new Date().getTime()}${file.originalname}`)
      }
    })
  }))
  update(@Param('id') id: string, @Body() updateProductDto: UpdateProductDto,@UploadedFile() image) {
    return this.productsService.update(+id, updateProductDto,image);
  }

  @Delete(':id')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  remove(@Param('id') id: string) {
    return this.productsService.remove(+id);
  }
  @Get('img/:imgpath')
  seeUploadedFile(@Param('imgpath') image, @Res() res) {
    console.log(image)
    return res.sendFile(image, { root: './image' });
  }
}
