import {Column, Entity, PrimaryGeneratedColumn} from "typeorm";

@Entity()
export class Product {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column('double')
    price: number;

    @Column()
    description: string;

    @Column({default:''})
    image:string;
}
